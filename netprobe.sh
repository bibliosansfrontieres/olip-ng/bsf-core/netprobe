#!/bin/bash

# shellcheck source=defaults
[ -r /etc/default/netprobe ] \
    && . /etc/default/netprobe

NETPROBE_DELAY="${NETPROBE_DELAY:-30}"
NETPROBE_LAN_INTERFACE="${NETPROBE_LAN_INTERFACE:-eth0}"
NETPROBE_CDN_TARGET="${NETPROBE_CDN_TARGET:-cdn-ideascube.local}"
NETPROBE_HTTP_TARGET="${NETPROBE_HTTP_TARGET:-https://detectportal.firefox.com/success.txt}"
NETPROBE_VPN_TARGET="${NETPROBE_VPN_TARGET:-10.90.100.254}"
NETPROBE_FLAGS_DIR="${NETPROBE_FLAGS_DIR:-/olip-files/network}"
NETPROBE_VERBOSE="${NETPROBE_VERBOSE:-0}"

say() {
    [ "$NETPROBE_VERBOSE" != 0 ] && echo >&2 "$*"
}

function test_interface() {
    output=$( cat "/sys/class/net/$NETPROBE_LAN_INTERFACE/operstate" )
    test "$output" = "up"
    return $?
}
function test_cdn() {
    ping -c 1 "$NETPROBE_CDN_TARGET" &> /dev/null
    return $?
}
function test_http() {
    local output
    output=$( wget --quiet "$NETPROBE_HTTP_TARGET" --timeout=5 -O- 2> /dev/null )
    test "$output" = "success"
    return $?
}
function test_vpn() {
    ping -c 1 "$NETPROBE_VPN_TARGET" &> /dev/null
    return $?
}

while true ; do

    mkdir -p "$NETPROBE_FLAGS_DIR"

    if ! test_interface ; then
        say "Interface down"
        rm -f "$NETPROBE_FLAGS_DIR/lan" "$NETPROBE_FLAGS_DIR/cdn" "$NETPROBE_FLAGS_DIR/internet"
    else

        say "Interface up"
        ip -brief address show "$NETPROBE_LAN_INTERFACE" | awk '{ print $3 }' | cut -d / -f 1 \
            > "$NETPROBE_FLAGS_DIR/lan"

        if test_cdn ; then
            say "CDN available"
            ping -c 1 "$NETPROBE_CDN_TARGET" | awk '/^PING/ { print $3 }' | tr -d '()' \
                > "$NETPROBE_FLAGS_DIR/cdn"
        else
            say "CDN not found"
            rm -f "$NETPROBE_FLAGS_DIR/cdn"
        fi

        if test_http ; then
            say "Internet available"
            touch "$NETPROBE_FLAGS_DIR/internet"
        else
            say "Internet not found"
            rm -f "$NETPROBE_FLAGS_DIR/internet"
        fi

        if test_vpn ; then
            say "VPN available"
            ip -brief route get "$NETPROBE_VPN_TARGET" | awk '/'"${NETPROBE_VPN_TARGET}"'/ { print $5 }' \
                > "$NETPROBE_FLAGS_DIR/vpn"
        else
            say "VPN not found"
            rm -f "$NETPROBE_FLAGS_DIR/vpn"
        fi

        chmod a+rX -R "$NETPROBE_FLAGS_DIR"

    fi  # end test_interface

    sleep "$NETPROBE_DELAY"

done
