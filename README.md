# netprobe

A service to check for network statuses.

## Synopsis

The script checks for the interface status.

If it is up, then it checks for CDN presence,
and downloads from a SSL backed URL
to ensure internet connectivity is functional.

## Flag files

The probes write flag files in order
to provide information to other services:

- `lan`: a local network is available
- `cdn`: a CDN server is available
- `internet`: a functional internet connection is available
- `vpn`: a functional VPN connection is available

Files are deleted as the related resource disappear.

This can be used, for instance in a Systemd unit:

```ini
[Unit]
Description = Updates download service
ConditionPathExists = /olip-files/network/internet
```

The `lan`, `cdn` and `vpn` flagfiles contain their respective IP addresses:

```text
$ cat /olip-files/network/cdn
10.93.1.16
```

## Usage

There is two ways to use this tool:

- The host install is powered by a systemd unit and a configuration file.
- The Docker image is configured via environment variables.

### Install on the host

The `install.sh` script
installs the binary,
creates a state directory,
installs a default configuration file,
installs and enables a systemd unit.

The unit is NOT started, it's up to you to start it whenever it is relevant:

```shell
systemctl enable netprobe.service
systemctl status netprobe.service
journalctl -f -u netprobe
```

#### Upgrade from the host

Just run the `install.sh` script again.

Note that the changes made to `/etc/default/netprobe` will be overwritten.

#### Upgrade from a docker container

We want to upgrade the script - installed on the host - from a container.

In order to do so, the container shall bind-mount the host's filesystem
(such as `/:/host`), then the `install.sh` script can be used with a `PREFIX`:

```shell
PREFIX=/host ./install.sh
```

When doing so, the script will NOT test whether `ping(1)` and `wget(1)` are available,
and it will NOT tell `systemd` to reload the (maybe updated) unit file.

### Docker usage

The container requires `net=host` in order to see the interfaces.
As a result, the container gets the same hostname than the host,
so it may be convenient to set the container `hostname` in order
to avoid confusion.

The container also needs to access the Avahi socket
in order to resolve mDNS casts

#### Docker run

```shell
docker run -d \
    --name netprobe \
    --hostname ct-netprobe \
    --net=host \
    -v /var/run/avahi-daemon/socket:/var/run/avahi-daemon/socket \
    -e NETPROBE_DELAY=3 \
    registry.gitlab.com/bibliosansfrontieres/olip-ng/bsf-core/netprobe:latest
```

#### Compose

```yaml
services:
  netprobe:
    container_name: netprobe
    hostname: ct-netprobe
    network_mode: host
    volumes:
        - /var/run/avahi-daemon/socket:/var/run/avahi-daemon/socket
    environment:
        - NETPROBE_DELAY=3
    image: registry.gitlab.com/bibliosansfrontieres/olip-ng/bsf-core/netprobe:latest
```

## Configuration

The script has a default configuration.

- on host installs, a custom configuration may be loaded from `/etc/default/netprobe`.
- on Docker install, you should use `env-file`/`environment` statements.

In both cases, values can be overriden via environment variables:

- `NETPROBE_FLAGS_DIR`: the directory where the state flags files are written to
- `NETPROBE_DELAY`: delay between probes, in seconds
- `NETPROBE_LAN_INTERFACE`: which interface the state should be checked for
- `NETPROBE_VERBOSE`: enable verbose mode for debugging
- `NETPROBE_CDN_TARGET`: the CDN hostname to search for
- `NETPROBE_HTTP_TARGET`: a SSL enabled URL to check for
- `NETPROBE_VPN_TARGET`: the VPN target to check for
