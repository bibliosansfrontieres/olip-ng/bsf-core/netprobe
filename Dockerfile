FROM debian:12-slim

# hadolint ignore=DL3008
RUN apt-get --quiet --quiet update \
    && apt-get install --quiet --quiet --yes --no-install-recommends \
        ca-certificates \
        iproute2 \
        iputils-ping \
        libnss-mdns \
        wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*


COPY netprobe.sh /usr/local/sbin/

ENV NETPROBE_VERBOSE=yes

CMD ["netprobe.sh"]
