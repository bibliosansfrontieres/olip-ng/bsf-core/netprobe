#!/bin/bash

[ -z "$PREFIX" ] && {
    type -t ping &> /dev/null || apt install --quiet --quiet --yes iputils-ping
    type -t wget &> /dev/null || apt install --quiet --quiet --yes wget
}

install --mode=0644 -d "${PREFIX}/olip-files/network"


install --mode=0754 netprobe.sh "${PREFIX}/usr/local/sbin/"
install --mode=0644 defaults "${PREFIX}/etc/default/netprobe"

install --mode=0644 netprobe.service "${PREFIX}/etc/systemd/system/"
[ -z "$PREFIX" ] && {
    systemctl --system daemon-reload >/dev/null
    systemctl enable netprobe.service
}
